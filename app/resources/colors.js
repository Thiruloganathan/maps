const Colors = {
  primaryColor: '#32a8fa',
  secondaryColor: '#2ecffa',
  colorWhite: '#ffffff',
  colorTrans: 'transparent',

  //splash screen
  darkGreen: '#07272d',
  middleGreen: '#033b40',
  lightGreen: '#4e6769',
  brownishOrange: '#da4e1f',
  yellow: '#faf3de',
  gray: '#d0d3d4',
};

export default Colors;
