import {StyleSheet, Dimensions, StatusBar, Platform} from 'react-native';
import Colors from './colors';
export const {width, height} = Dimensions.get('window');
export const margin = width / 25;
export const VALUE10 = 10,
  VALUE20 = 20,
  VALUE15 = 15,
  VALUE17 = 17;
import {getStatusBarHeight} from '../utills/StatusBar';

export const STATUSBAR_HEIGHT =
  Platform.OS === 'ios' ? getStatusBarHeight() : StatusBar.currentHeight;

const styles = StyleSheet.create({
  //app.js
  app_flex_1: {
    flex: 1,
  },
  app_container: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  app_btn: (btn_clr) => ({
    backgroundColor: btn_clr,
    paddingHorizontal: VALUE15,
    paddingVertical: VALUE15,
    borderRadius: 30,
    marginTop: VALUE15,
  }),
  app_txt: {
    fontSize: 15,
    fontWeight: '600',
    color: Colors.colorWhite,
  },

  //components
  //header.js
  header_container: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header_pop: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    marginStart: VALUE10,
  },
  header_txt: {
    flex: 1,
    fontSize: 23,
    fontWeight: '600',
    marginHorizontal: VALUE10,
  },

  //custom_marker.js
  marker_container: {
    height: 35,
    width: 35,
  },
  marker_img: {
    height: 35,
    width: 35,
    resizeMode: 'contain',
  },

  //screens
  //map.js
  map_container: {
    flex: 1,
  },
  map_view: {
    flex: 1,
  },

  //splash.js
  splash_container: {
    flex: 1,
    backgroundColor: '#07272d',
    justifyContent: 'center',
    alignItems: 'center',
  },
  splash_logo: {
    width: '100%',
    height: '40%',
    resizeMode: 'center',
    marginTop: '15%',
  },
  splash_labels: {
    width: '100%',
    height: '30%',
    resizeMode: 'center',
  },

  //terms.js
  terms_container: {
    flex: 1,
    marginHorizontal: margin * 2,
    marginVertical: margin,
    backgroundColor: Colors.colorWhite,
  },
  terms_title_txt: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: '600',
    marginBottom: margin,
  },
  terms_content_txt: {
    fontSize: 17,
  },
  terms_btn_view: {
    backgroundColor: Colors.colorWhite,
    bottom: 0,
    width: '100%',
    height: '15%',
    justifyContent: 'center',
    alignItems: 'center',
    padding: VALUE15,
  },
  terms_btn: {
    width: '90%',
    backgroundColor: Colors.middleGreen,
    paddingHorizontal: VALUE17,
    paddingVertical: VALUE20,
    borderRadius: VALUE10,
    alignItems: 'center',
  },
  terms_btn_txt: {
    color: Colors.colorWhite,
    fontWeight: '600',
    fontSize: 16,
  },

  //welcome slider
  slider_container: {
    width: '100%',
    height: height - 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  slider_txt_title: {
    fontSize: 25,
    fontWeight: '700',
    textAlign: 'center',
    marginHorizontal: VALUE10,
    marginTop: VALUE15,
  },
  slider_txt: (index, length) => ({
    fontSize: 17,
    textAlign: 'center',
    marginHorizontal: VALUE20,
    marginVertical: index === length - 1 ? VALUE20 : VALUE15,
  }),
  slider_img: (index, length) => ({
    width: width,
    height: index === length - 1 ? width / 1.5 : width / 1.25,
    resizeMode: 'contain',
  }),
  slider_content: (index) => ({
    width: '100%',
    justifyContent: 'center',
    marginHorizontal: VALUE15,
    paddingVertical: VALUE10,
  }),
  slider_bottom_space: {height: margin, justifyContent: 'center'},
  slider_btn_view: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: VALUE10,
  },
  slider_btn: {
    width: '90%',
    backgroundColor: Colors.middleGreen,
    paddingHorizontal: VALUE17,
    paddingVertical: VALUE15,
    borderRadius: VALUE10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  slider_last_img_view: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  slider_last_img: {
    width: width / 4,
    height: width / 5,
    resizeMode: 'contain',
  },
});

export default styles;
