export const locations = [
  {
    title: 'Location 1',
    latitude: 24.86170245,
    longitude: 67.00310938,
    marker_image: require('../assets/location.png'),
    weight: 13,
  },
  {
    title: 'Location 2',
    latitude: 24.8317098,
    longitude: 67.00210948,
    marker_image: require('../assets/location.png'),
    weight: 19,
  },
  {
    title: 'Location 3',
    latitude: 24.83073537,
    longitude: 67.02129903,
    marker_image: require('../assets/location.png'),
    weight: 18,
  },
  {
    title: 'Location 4',
    latitude: 24.83073537,
    longitude: 67.03129903,
    marker_image: require('../assets/location.png'),
    weight: 2,
  },
  {
    title: 'Location 5',
    latitude: 24.83073537,
    longitude: 67.05139903,
    marker_image: require('../assets/location.png'),
    weight: 12,
  },
];

export const terms =
  "To consume alcohol on this tour you must be 19 or older, which is the age of majority in the Province of Ontario. \n\n If you are drinking, please do so responsibly. \n\nPlease be aware of traffic when crossing the road. Keep in mind traffic may approach from a different direction than what you're used to. \n\nMake sure you obey all rules of the road, and cross only at designated pedestrian crossing locations. \n\nThey say there are only two seasons in Toronto; winter and construction! The City of Toronto may unexpectedly close certain roads, which is unfortunately beyond our control. Please use the GPS function in the app to find a suitable diversion or follow any detour signs that may have been provided. \n\nToronto is generally a safe city, but please travel aware, keep your  head up and a firm grip on your phone at all times.Some establishments may still be under COVID-19 related restrictions. Please follow signage and direction of staff where applicable.GPS-based apps can cause a drain on your battery. Please the toggle switch in the app to turn off GPS when not required and preserve battery life.";
