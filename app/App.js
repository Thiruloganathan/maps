import React, {Component} from 'react';
import {View} from 'react-native';
import AppNavigator from './router/index';
import Styles from './resources/styles';
import Colors from './resources/colors';
import MyStatusBar from './components/MyStatusBar';

export default class App extends Component {
  render() {
    return (
      <View style={Styles.app_flex_1}>
        <AppNavigator />
      </View>
    );
  }
}
