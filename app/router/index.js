import React, {Component} from 'react';
import {Easing, Animated} from 'react-native';
import {Router, Scene} from 'react-native-router-flux';
import Home from '../screens/home/index';
import Map from '../screens/map';
import MapBox from '../screens/map_box';

import SplashScreen from '../screens/splash';
import TermsScreen from '../screens/terms';
import WelcomeScreen from '../screens/WelcomeSlider';

const MyTransitionSpec = {
  duration: 1000,
  easing: Easing.bezier(0.2833, 0.99, 0.31833, 0.99),
  timing: Animated.timing,
};

const transitionConfig = () => ({
  transitionSpec: MyTransitionSpec,
  screenInterpolator: (sceneProps) => {
    const {layout, position, scene} = sceneProps;
    const {index} = scene;
    const width = layout.initWidth;

    //right to left by replacing bottom scene
    return {
      transform: [
        {
          translateX: position.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: [width, 0, -width],
          }),
        },
      ],
    };
  },
});

export default class index extends Component {
  render() {
    return (
      <Router>
        <Scene key="root" transitionConfig={transitionConfig}>
          <Scene key="home" component={Home} hideNavBar initial />
          <Scene key="map" component={Map} hideNavBar />
          <Scene key="map_box" component={MapBox} hideNavBar />
          <Scene key="splash" component={SplashScreen} hideNavBar />
          <Scene key="terms" component={TermsScreen} hideNavBar />
          <Scene key="welcome" component={WelcomeScreen} hideNavBar />
        </Scene>
      </Router>
    );
  }
}
