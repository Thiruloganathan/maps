import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import Styles from '../resources/styles';
import {Actions} from 'react-native-router-flux';

export default class header extends Component {
  render() {
    const {title, isBack, onBackPress} = this.props;
    return (
      <View style={Styles.header_container}>
        {isBack && (
          <TouchableOpacity
            onPress={onBackPress ? onBackPress : () => Actions.pop()}>
            <Image
              source={require('../resources/assets/back_arrow.png')}
              style={Styles.header_pop}
            />
          </TouchableOpacity>
        )}
        <Text style={Styles.header_txt}> {title} </Text>
      </View>
    );
  }
}
