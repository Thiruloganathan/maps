import React, {memo} from 'react';
import {View, Platform, StatusBar, StyleSheet} from 'react-native';
import Colors from '../resources/colors';
import {getStatusBarHeight} from '../utills/StatusBar';

const STATUSBAR_HEIGHT =
  Platform.OS === 'ios' ? getStatusBarHeight() : StatusBar.currentHeight;
const styles = StyleSheet.create({
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
});

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, {backgroundColor}]}>
    <StatusBar
      translucent
      barStyle={'light-content'}
      backgroundColor={backgroundColor}
      {...props}
    />
  </View>
);

export default memo(MyStatusBar);
