import React, {Component} from 'react';
import {View, Image} from 'react-native';
import Styles from '../resources/styles';

export default class custom_marker extends Component {
  render() {
    const {item} = this.props;
    return (
      <View style={Styles.marker_container}>
        <Image source={item.marker_image} style={Styles.marker_img} />
      </View>
    );
  }
}
