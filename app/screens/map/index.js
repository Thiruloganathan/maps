import React, {Component} from 'react';
import {SafeAreaView, View, Text, Animated} from 'react-native';
import Styles from '../../resources/styles';
import Colors from '../../resources/colors';
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
  Heatmap,
  Circle,
  Polyline,
  Polygon,
} from 'react-native-maps';
import Header from '../../components/header';
import CustomMarker from '../../components/custom_marker';
import {Actions} from 'react-native-router-flux';

import {locations} from '../../resources/data/data';

export default class index extends Component {
  state = {
    tracksViewChanges: false,
  };

  shouldComponentUpdate(nextProps) {
    if (this.props.activities !== nextProps.activities) {
      this.setState({tracksViewChanges: false});
    }
    return true;
  }

  render() {
    const {pos, onSelect} = this.props;
    return (
      <SafeAreaView style={Styles.map_container}>
        <Header
          title={'Map Screen'}
          isBack={true}
          onBackPress={() => {
            var updatedPos = pos < locations.length ? pos + 1 : 1;
            onSelect(updatedPos);
            Actions.pop();
          }}
        />

        <MapView
          provider={PROVIDER_GOOGLE}
          tracksViewChanges={this.state.tracksViewChanges}
          style={Styles.map_view}
          initialRegion={{
            latitude: 24.83073537,
            longitude: 67.02129903,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}>
          {locations.slice(0, pos).map((marker, index) => (
            <View key={index}>
              <Polyline
                coordinates={locations.slice(0, pos)}
                key={index}
                strokeWidth={1}
                strokeColor={Colors.primaryColor}
                // lineDashPattern={[150, 150]}
              />
              <Marker
                key={marker.weight}
                coordinate={{
                  latitude: marker.latitude,
                  longitude: marker.longitude,
                }}>
                <CustomMarker item={marker} />
              </Marker>
            </View>
          ))}
        </MapView>
      </SafeAreaView>
    );
  }
}
