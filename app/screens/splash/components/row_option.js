import React, {memo} from 'react';
import {View, Text, Image} from 'react-native';

const RowOption = ({img_src, txt}) => {
  return (
    <View style={{flex: 1, flexDirection: 'row'}}>
      <Image
        source={require('../../../resources/assets/ic_location.png')}
        style={{width: 30, height: 30, resizeMode: 'contain'}}
      />
      <Text style={{color: '#faf3de', fontWeight: '700'}}>{txt}</Text>
    </View>
  );
};

export default memo(RowOption);
