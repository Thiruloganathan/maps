import React, {memo, useEffect} from 'react';
import {SafeAreaView, View, Image} from 'react-native';
import Styles from '../../resources/styles';
import Colors from '../../resources/colors';
import MyStatusBar from '../../components/MyStatusBar';
import {Actions} from 'react-native-router-flux';

const Terms = ({}) => {
  useEffect(() => {
    // Anything in here is fired on component mount.
    setTimeout(() => {
      Actions.welcome();
    }, 2000);
    return () => {
      // Anything in here is fired on component unmount.
    };
  }, []);

  return (
    <View style={Styles.app_flex_1}>
      <MyStatusBar backgroundColor={Colors.darkGreen} />
      <SafeAreaView style={Styles.splash_container}>
        <Image
          source={require('../../resources/assets/ic_logo.png')}
          style={Styles.splash_logo}
        />
        <Image
          source={require('../../resources/assets/ic_labels.png')}
          style={Styles.splash_labels}
        />
      </SafeAreaView>
    </View>
  );
};

export default memo(Terms);
