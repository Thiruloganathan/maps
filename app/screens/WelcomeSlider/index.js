import React, {Component} from 'react';
import AppIntroSlider from './AppIntroSlider';
import {
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Styles from '../../resources/styles';
import Colors from '../../resources/colors';
import {Actions} from 'react-native-router-flux';
import MyStatusBar from '../../components/MyStatusBar';
export const {width, height} = Dimensions.get('window');

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slides: [
        {
          key: 'k1',
          title: 'Welcome to Thirst \nfor Facts!',
          text:
            'The coolest way to explore both the famous, and less well-known sights of a city - at your own pace, and with plenty of stops to sample refreshing local brews!',
          image: require('../../resources/assets/ic_welcome_slider_1.png'),
          img_additional: '',
        },
        {
          key: 'k2',
          title: 'Intriguing facts and \nlively trivia',
          text:
            "You'll have loads of fun discovering the city on foot, with this interactive, GPS-driven tour, featuring intriguing facts and lively trivia!",
          image: require('../../resources/assets/ic_welcome_slider_2.png'),
          img_additional: '',
        },
        {
          key: 'k3',
          title: '',
          text:
            "All tours include samples of local beers at our partner pubs, and an exclusive, branded sampling glass, that's yours to keep!",
          image: require('../../resources/assets/ic_welcome_slider_3.png'),
          img_additional: require('../../resources/assets/ic_welcome_slider_3.1.png'),
        },
      ],
    };
  }
  _renderItem = ({item, index}) => {
    const {slides} = this.state;
    return (
      <View style={{flex: 1}}>
        <View style={Styles.slider_container}>
          <Image
            source={item.image}
            style={Styles.slider_img(index, slides.length)}
          />
          {index < slides.length - 1 && (
            <Text
              style={Styles.slider_txt_title}
              numberOfLines={2}
              adjustsFontSizeToFit>
              {item.title}
            </Text>
          )}
          {index === slides.length - 1 && (
            <Image
              source={item.img_additional}
              style={Styles.slider_last_img}
            />
          )}
          <Text
            style={Styles.slider_txt(index, slides.length)}
            numberOfLines={4}
            adjustsFontSizeToFit>
            {item.text}
          </Text>
          {index === slides.length - 1 && (
            <View style={Styles.slider_btn_view}>
              <TouchableOpacity
                style={Styles.slider_btn}
                onPress={() => Actions.replace('terms')}>
                <Text style={Styles.terms_btn_txt}>{'CONTINUE'}</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
        <View style={{width: '100%', height: 100}} />
      </View>
    );
  };
  render() {
    return (
      <View style={Styles.app_flex_1}>
        <MyStatusBar backgroundColor={Colors.colorTrans} />
        <SafeAreaView style={Styles.app_flex_1}>
          <AppIntroSlider
            data={this.state.slides}
            onSkip={() => Actions.replace('terms')}
            renderItem={this._renderItem}
            showSkipButton
            showNextButton
            showDoneButton={false}
            activeDotStyle={{backgroundColor: Colors.middleGreen}}
          />
        </SafeAreaView>
      </View>
    );
  }
}
