import React, {memo} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Styles from '../../resources/styles';
import Colors from '../../resources/colors';
import MyStatusBar from '../../components/MyStatusBar';
import {Actions} from 'react-native-router-flux';

import {terms} from '../../resources/data/data';

const index = ({}) => {
  return (
    <View style={Styles.app_flex_1}>
      <MyStatusBar backgroundColor={Colors.colorWhite} />
      <ScrollView>
        <SafeAreaView style={Styles.terms_container}>
          <View>
            <Text style={Styles.terms_title_txt}>{'Terms and conditions'}</Text>
            <Text style={Styles.terms_content_txt}>{terms}</Text>
          </View>
        </SafeAreaView>
      </ScrollView>
      <View style={Styles.terms_btn_view}>
        <TouchableOpacity
          style={Styles.terms_btn}
          onPress={() => Actions.map_box()}>
          <Text style={Styles.terms_btn_txt}>{"I'M COOL WITH ALL THAT!"}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default memo(index);
