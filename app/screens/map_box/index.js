import React, {Component} from 'react';
import {SafeAreaView, Text} from 'react-native';
import Styles from '../../resources/styles';
import Header from '../../components/header';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <SafeAreaView
        style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text style={Styles.terms_title_txt}>Home</Text>
      </SafeAreaView>
    );
  }
}
