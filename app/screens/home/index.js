import React, {Component} from 'react';
import {SafeAreaView, Text, View, TouchableOpacity} from 'react-native';
import Styles from '../../resources/styles';
import Colors from '../../resources/colors';
import {Actions} from 'react-native-router-flux';
import Header from '../../components/header';

export default class index extends Component {
  state = {
    pathPos: 1,
  };

  renderBtn = (index, txt) => {
    return (
      <TouchableOpacity
        style={Styles.app_btn(Colors.primaryColor)}
        onPress={() => {
          index == 0
            ? Actions.map({
                pos: this.state.pathPos,
                onSelect: (pos) => {
                  this.setState({pathPos: pos});
                },
              })
            : Actions.map_box();
        }}>
        <Text style={Styles.app_txt}>{txt}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <SafeAreaView style={Styles.app_flex_1}>
        <Header title={'Home'} isBack={false} />
        <View style={Styles.app_container}>
          {this.renderBtn(0, 'Go To Maps screen')}
          {this.renderBtn(1, 'Go To Map box screen')}
        </View>
      </SafeAreaView>
    );
  }
}
